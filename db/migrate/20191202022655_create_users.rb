class CreateUsers < ActiveRecord::Migration[6.0]
  def up
    create_table :users do |t|
      # t.column "first_name", :string
      t.string "name", :limit => 50
      t.string "email", :default => '', :null => false
      t.string "password", :default => 40

      t.timestamps
    end
  end

  def down
    drop_table :users
  end
end
