class ChangePagePermalinkToString < ActiveRecord::Migration[6.0]
  def up
    change_column("pages", "permalink", :string, :default => '')
  end

  def down
    change_column("pages", "permalink", :integer)
  end
end
