class DemoController < ApplicationController
  layout false

  def index
    render('index') # this is the default action
  end

  # note if this doesn't exist, 
  # then it seems to just return the default associated view
  def hello
    @id = params[:id]
    @page = params[:page]
    
    @name = ['Bob', 'Me', 'Hello', 'Blarg'].shuffle[0]
  end

  def other_hello
    #redirect_to(:controller => 'demo', :action => 'hello')
    redirect_to(:action => 'hello') # seems to be a 302 redirect
  end

  def google
    # seems the https portion is needed to indentify it as hardcoded
    redirect_to('https://www.google.com')
  end
end
