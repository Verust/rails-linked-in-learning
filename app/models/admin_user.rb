class AdminUser < ApplicationRecord

    has_secure_password

    scope :sorted, lambda {order("name ASC")}

    has_and_belongs_to_many :pages
    has_many :section_edits
    has_many :sections, :through => :section_edits

    EMAIL_REGEX = /\A[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}\z/i
    FORBIDDEN_USER_NAME = ['I am not allowed', 'Pancake']

    validates :name, :presence => true,
        :length => {:maximum => 75}
    
    validates :username, :presence => true,
        :length => {:within => 8..25},
        :uniqueness => true

    validates :email, :presence => true,
        :length => {:maximum => 100},
        :format => {:with => EMAIL_REGEX},
        :confirmation => true

    validate :username_is_allowed

    private 

    def username_is_allowed
        if FORBIDDEN_USER_NAME.include?(username)
            errors.add(
                :username, 
                "cannot be: '#{FORBIDDEN_USER_NAME.join('\' or \'')}'."
            )
        end
    end
end
